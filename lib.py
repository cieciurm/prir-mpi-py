def f(x):
		return x*x

def divide_ox(start, end, n_points):
	"""dzieli przedzial Xow (start;end) na n_points 
	rownoodleglych punktow"""
	ox = []

	step = float((end - start)) / (n_points-1)

	ox.append(start)	

	for i in range(1, n_points):
		ox.append(ox[i-1] + step)

	return ox

def create_ranges(n_points, n_proc):
	"""tworzy tablice range'ow, na podstawie liczby punktow
	i liczby procesow wykonujacych"""
	ranges = []
	steps = []

	step = n_points / n_proc
	remainder = n_points % n_proc

	for i in range(n_proc):
		steps.append(step)
		if remainder > 0:
			steps[i] += 1
			remainder -= 1
	
	tmp = 0

	for i in range(n_proc):
        	tmp += steps[i]
		ranges.append(tmp)
	return ranges

def trap_integrate(x):
	"""liczby calke metoda trapezow dla listy Xow podanych
	w liscie x"""
	n_points = len(x)
	n_trap = n_points - 1
	sum = 0

	y = []

	for i in range(n_trap):
		y.append(f(x[i]))

	#print "n points=", n_points
	#print "n trap=", n_trap

	for i in range(n_trap):
		tmp = ((f(x[i]) + f(x[i+1])) * (x[i+1]-x[i])) / 2
		sum += tmp

	return sum
