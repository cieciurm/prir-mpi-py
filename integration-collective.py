import sys
from lib import *
from mpi4py import MPI

if len(sys.argv) < 3:
	print "python integration.py [start] [end] [n-points]"
	sys.exit()

start = float(sys.argv[1])
end = float(sys.argv[2])
n_points = int(sys.argv[3])

comm = MPI.COMM_WORLD
n_proc = comm.Get_size()
current_proc = comm.Get_rank()

if n_points < 2:
	print "Min. 2 integration points!"
	sys.exit()

ox = []

if current_proc == 0:
	# dzielimy punkty na n rownych czesci
	all_ox = divide_ox(start, end, n_points)
	# tworzymy liste z podzialem pracy pomiedzy procesy
	ranges = create_ranges(n_points, n_proc)

	for i in range(n_points):
		if i == 0:
			s = 0
		else:
			s = ranges[i-1] - 1
		if i >= len(ranges):
			break
		e = ranges[i]

		ox.append(all_ox[s:e])

ox = comm.scatter(ox)

local_integrate = trap_integrate(ox)

#print local_integrate
local_integrate_list = comm.gather(local_integrate, root=0)

if current_proc == 0:
	total = 0
	for i in range(len(local_integrate_list)):
		total += local_integrate_list[i]
	print "Total integral=",total
