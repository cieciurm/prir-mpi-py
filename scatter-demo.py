from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
   data = [(i+1)**2 for i in range(size)]
else:
   data = None

print "[pre-scatter] rank:",rank,"data:",data
data = comm.scatter(data)

print "[post-scatter] rank:",rank,"data:",data
