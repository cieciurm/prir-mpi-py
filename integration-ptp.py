import sys
from lib import *
from mpi4py import MPI

if len(sys.argv) < 3:
	print "python integration.py [start] [end] [n-points]"
	sys.exit()

start = float(sys.argv[1])
end = float(sys.argv[2])
n_points = int(sys.argv[3])

comm = MPI.COMM_WORLD
n_proc = comm.Get_size()

if n_points < 2:
	print "Min. 2 integration points!"
	sys.exit()

# dzielimy punkty na n rownych czesci
ox = divide_ox(start, end, n_points)

# tworzymy liste z podzialem pracy pomiedzy procesy
ranges = create_ranges(n_points, n_proc)

current_proc = comm.Get_rank()

if current_proc == 0:
	s = 0
else:
	s = ranges[current_proc-1] - 1
e = ranges[current_proc]

current_ox = ox[s:e]
#print current_ox

result = trap_integrate(current_ox)

tmp = 0
if current_proc == 0:
	total = result
	for i in range(1, n_proc):
		tmp = comm.recv(source=i)
		total += tmp
else:
	comm.send(result, dest=0)

if current_proc == 0:
	print "Total integral=",total
