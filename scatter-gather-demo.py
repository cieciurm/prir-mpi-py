from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
	data = [(i+1)**2 for i in range(size)]
	# data to lista kwadratow (i+1)
	# musi byc tyle samo elementow, co size
	#
	# so far, tylko jeden proces na te liste
else:
	data = None

print "[pre-scatter] rank:",rank,"data:",data

data = comm.scatter(data)
# rozsylamy do wszystkich data
# teraz kazdy ma jeden element z listy

print "[post-scatter] rank:",rank,"data:",data

data *= 2
# kazdy sobie mnozy swoja wartosc, ktora dostal

data = comm.gather(data, root=0)
# teraz proces o rank=0
# ma z powrotem liste elementow

print "[post-gather] rank:",rank,"data:",data

if rank == 0:
	# jeden proces (ten ktory dostal wszystko) sumuje
	total = 0.0
	for i in range(len(data)):
		total += data[i]
	
	print "sum of data:",total
