#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "lib.h"

int main(int argc, char** argv) {
	double start, end, s, e, result, tmp, total;
	int n_points, n_proc, ierr, current_proc, i;
	double *ox, *current_ox;
	int *ranges;

	if (argc < 3) {
		printf("python integration.py [start] [end] [n-points]\n");
		return -1;
	}

	start = atof(argv[1]);
	end = atof(argv[2]);
	n_points = atoi(argv[3]);

	if (n_points < 2) {
		printf("Min. 2 integration points!\n");
		return -2;
	}

	ierr = MPI_Init(&argc, &argv);
	ierr = MPI_Comm_size(MPI_COMM_WORLD, &n_proc);

	/*dzielimy punkty na n rownych czesci*/
	ox = divide_ox(start, end, n_points);

	/*tworzymy liste z podzialem pracy pomiedzy procesy*/
	ranges = create_ranges(n_points, n_proc);

	ierr = MPI_Comm_rank(MPI_COMM_WORLD, &current_proc);

	if (current_proc == 0)
		s = 0;
	else
		s = ranges[current_proc-1] - 1;

	e = ranges[current_proc];

	current_ox = get_part_vector(ox, s, e);

	result = trap_integrate(current_ox, (e-s));

	tmp = 0;
	if (current_proc == 0) {
		total = result;
		for (i = 1; i < n_proc; i++) {
			MPI_Recv(&tmp, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			total += tmp;
		}
	} else {
		MPI_Send(&result, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}

	if (current_proc == 0)
		printf("Total integral=%g\n", total);

	/* 
	 * to trzeba zwolnic tu (bo wszyscy to maja), ale wywala blad(?)
	 */
	free(ox);
	free(current_ox);
	free(ranges);

	ierr = MPI_Finalize();
	
	return 0;
}
