#include <stdlib.h>
#include <stdio.h>
#include "lib.h"

double f(double x) {
	return x*x;
}

double *divide_ox(double start, double end, int n_points) {
	double step;
	double *ox;
	int i;
	
	ox = malloc(n_points * sizeof(double));

	step = (end - start) / (n_points-1);

	ox[0] = start;

	for (i = 1; i < n_points; i++)
		ox[i] = ox[i-1] + step;

	return ox;
}

/* tworzy tablice punktow uwzgledniajac zakladki */
/* uzupelnia tablice liczby elementow kazdego podprocesu */
double *create_all_ox(int *ranges, double *ox, int *sizes, int n_points, int n_proc) {
	int n_overlap, all_ox_size, size, i, j, k, s, e;
	double *all_ox, *part_vector, *tmp;

	n_overlap = n_proc - 1;

	all_ox_size = n_points + n_overlap;

	all_ox = malloc(all_ox_size * sizeof(double));

	k = 0;
	for (i = 0; i < n_proc; i++) {

		if (i == 0) {
			s = 0;
		} else {
			s = ranges[i-1] - 1;
		}
		e = ranges[i];

		size = e - s;
		/* ile jest elementow w aktualnej czesci */
		*(sizes + i) = size;

		/*printf("[i=%d] start: %d, end: %d, size: %d\n", i, s, e, size);*/

		tmp = get_part_vector(ox, s, e);

		for (j = 0; j < size; j++) {
			all_ox[k] = tmp[j];
			k++;
		}
		free(tmp);
	}
	return all_ox;
}

int *create_indexes(int *sizes, int n_proc) {
	int i, total_sizes;
	int *indexes;

	indexes = malloc(n_proc * sizeof(int));
	total_sizes = 0;

	for (i = 0; i < n_proc; i++) {
		if (i == 0)
			indexes[i] = 0;
		else
			indexes[i] = total_sizes;

		total_sizes += sizes[i];
	}

	return indexes;
}

int *create_ranges(int n_points, int n_proc) {
	int *ranges, *steps;
	int step, remainder, i, tmp;

	ranges = malloc(n_proc * sizeof(int));
	steps = malloc(n_proc * sizeof(int));

	step = n_points / n_proc;
	remainder = n_points % n_proc;
	
	for (i = 0; i < n_proc; i++) {
		steps[i] = step;
		if (remainder > 0) {
			steps[i]++;
			remainder--;
		}
	}
	
	tmp = 0;

	for (i = 0; i < n_proc; i++) {
		tmp += steps[i];
		ranges[i] = tmp;
	}

	if(n_proc > 1)
		i = ranges[1] - ranges[0] + 1;
	else 
		i = ranges[0];
	
	free(steps);
	return ranges;
}

double trap_integrate(double* x, int size) {
	int n_points, n_trap, i;
	double *y;
	double tmp, sum;
	
	n_points = size;
	n_trap = n_points - 1;
	sum = 0;

	y = malloc(size * sizeof(double));

	for (i = 0; i < n_trap; i++)
		y[i] = f(x[i]);

	for (i = 0; i < n_trap; i++) {
		tmp = ((f(x[i]) + f(x[i+1])) * (x[i+1]-x[i])) / 2;
		sum += tmp;
	}
	free(y);
	return sum;
}

double* get_part_vector(double *vector, int start, int end) {
	double *part_vector;
	int i, j, size;

	size = end - start;

	part_vector = malloc(size * sizeof(double));

	j = 0;
	for (i = start; i < end; i++) {
		part_vector[j] = vector[i];
		j++;
	}	
	return part_vector;
}

void print_double_array(double *t, int size, int rank) {
	int i;

	if (rank != -1)
		printf("Rank: %d\n", rank);

	for (i = 0; i < size; i++)
		printf("[%g]", t[i]);
	printf("\n");
}

void print_int_array(int *t, int size, int rank) {
	int i;

	if (rank != -1)
		printf("Rank: %d\n", rank);

	for (i = 0; i < size; i++)
		printf("[%d]", t[i]);
	printf("\n");
}
