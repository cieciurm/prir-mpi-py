#ifndef LIB_H
#define LIB_H

double f(double x);
double* divide_ox(double start, double end, int n_points);
int* create_ranges(int n_points, int n_proc);
double trap_integrate(double* x, int size);
double* get_part_vector(double *vector, int start, int end);
void print_array (double *row, int nElements);
double *create_all_ox(int *ranges, double *ox, int *sizes, int n_points, int n_proc);
int *create_indexes(int *sizes, int n_proc);
void print_double_array(double *t, int size, int rank);
void print_int_array(int *t, int size, int rank);

#endif
