#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "lib.h"

int main(int argc, char** argv) {
	double start, end, local_result, total_result;
	int n_points, n_proc, ierr, current_proc, i, j, local_n_points;
	double *ox, *all_ox, *current_ox, *partial_sums;
	int *ranges, *sizes, *indexes;

	if (argc < 3) {
		printf("[start] [end] [n-points]\n");
		return -1;
	}

	start = atof(argv[1]);
	end = atof(argv[2]);
	n_points = atoi(argv[3]);

	if (n_points < 2) {
		printf("Min. 2 integration points!\n");
		return -2;
	}

	ierr = MPI_Init(&argc, &argv);

	ierr = MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
	ierr = MPI_Comm_rank(MPI_COMM_WORLD, &current_proc);

	if (current_proc == 0) {

		ox = divide_ox(start, end, n_points);
#ifdef DEBUG
		printf("OX:\n");
		print_double_array(ox, n_points, -1);
#endif
		ranges = create_ranges(n_points, n_proc);

#ifdef DEBUG
		printf("RANGES:\n");
		print_int_array(ranges, n_proc, -1);
#endif

		/* 3 tablice - potrzebne do Scatterv */
		sizes = malloc(n_proc * sizeof(int));
		all_ox = create_all_ox(ranges, ox, sizes, n_points, n_proc);
		indexes = create_indexes(sizes, n_proc);

#ifdef DEBUG
		printf("ALL_OX:\n");
		print_double_array(all_ox, n_points+(n_proc-1), -1);
		printf("SIZES:\n");
		print_int_array(sizes, n_proc, -1);
		printf("INDEXES:\n");
		print_int_array(indexes, n_proc, -1);
#endif
	}

	/* rozsyłamy liczbe elementow wszystkim procesom */
	MPI_Scatter(sizes, 1, MPI_INT, &local_n_points, 1, MPI_INT, 0, MPI_COMM_WORLD);

#ifdef DEBUG
	printf("rank: %d, local points: %d\n", current_proc, local_n_points);
#endif

	current_ox = malloc(local_n_points * sizeof(double));

	/* rozsyłamy kawałki punktów do wszystkich */
	MPI_Scatterv(all_ox, sizes, indexes, MPI_DOUBLE, current_ox, 100, MPI_DOUBLE, 0, MPI_COMM_WORLD);

#ifdef DEBUG
	print_double_array(current_ox, local_n_points, -1);
#endif

	/* każdy proces liczby swoja podcałkę */
	local_result = trap_integrate(current_ox, local_n_points);

	if (current_proc == 0)
		partial_sums = malloc(n_proc * sizeof(double));

	MPI_Gather(&local_result, 1, MPI_DOUBLE, partial_sums, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	/* proces 0 zbiera wszystko i sumuje */
	if (current_proc == 0) {
		
#ifdef DEBUG
		printf("LOCAL SUMS:\n");
		print_double_array(partial_sums, n_proc, -1);
#endif

		total_result = 0;
		for (i = 0; i < n_proc; i++)
			total_result += partial_sums[i];

		printf("Total integral=%g\n", total_result);

		free(ox);
		free(all_ox);
		free(ranges);
		free(indexes);
		free(sizes);
		free(partial_sums);
	}

	/* 
	 * to trzeba zwolnic tu (bo wszyscy to maja), ale wywala blad(?)
	 */
	free(current_ox);

	ierr = MPI_Finalize();

	return 0;
}
