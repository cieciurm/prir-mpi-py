#!/bin/sh

START=1
END=2
POINTS=10
PROC=5

time mpiexec -n $PROC python integration-ptp.py $START $END $POINTS

time mpiexec -n $PROC python integration-collective.py $START $END $POINTS
